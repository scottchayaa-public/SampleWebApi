﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SampleWebApi.Responses.Github;

namespace SampleWebApi.Services.GitHub
{
    /// <summary>
    /// Github API Document
    /// https://docs.github.com/en/rest
    /// </summary>
    public class GithubService
    {
        private const string token = "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";
        private readonly HttpClient _httpClient;

        public GithubService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri("https://api.github.com");
            _httpClient.Timeout = TimeSpan.FromMilliseconds(3000);
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/vnd.github+json");
            //_httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token); // 若無 Authorization, 則會有 401 錯誤
            _httpClient.DefaultRequestHeaders.Add("User-Agent", "App_Scott"); // 若無 User-Agent, 則會有 403 錯誤
        }

        public async Task<UserResponse> GetUserAsync()
        {
            //var httpRequestMessage = new HttpRequestMessage();
            //httpRequestMessage.Method = HttpMethod.Get;
            //httpRequestMessage.Content = new StringContent("");
            //httpRequestMessage.RequestUri = new Uri("/user");

            //var response = await _httpClient.SendAsync(httpRequestMessage);

            var response = await _httpClient.GetAsync("/user");

            if (response.IsSuccessStatusCode == false)
            {
                throw new HttpClientException(response);
            }

            var body = await response.Content.ReadAsStringAsync();

            var json = JsonConvert.DeserializeObject<UserResponse>(body);

            if (json == null)
            {
                throw new FormatException("Json format result = null");
            }

            return json;
        }

    }
}
