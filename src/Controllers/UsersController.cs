﻿using AutoMapper;
using SampleWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using NLog;
using SampleWebApi.Responses;
using SampleWebApi.Requests.User;
using SampleWebApi.Domains;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IUserRepo _userRepo;
        private readonly IMapper _mapper;

        public UsersController(IUserRepo userRepo, IMapper mapper)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }


        [HttpGet]
        public ActionResult<IEnumerable<UserResponse>> GetUserAll([FromQuery] GetUsersQuery query)
        {
            var rows = _userRepo.GetMany(query);

            return Ok(_mapper.Map<IEnumerable<UserResponse>>(rows));
        }

        [HttpGet("{id}", Name = "User.GetById")]
        public ActionResult<UserResponse> GetById(int id)
        {
            var row = _userRepo.GetOne(id);

            if (row == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<UserResponse>(row));
        }

        [HttpPost]
        public ActionResult<UserResponse> CreateUser([FromBody] CreateBody body)
        {
            throw new Exception("測試一下丟 Exception 會怎麼樣"); // TODO: 故意丟 Exception

            var row = _mapper.Map<User>(body);
            _userRepo.CreateOne(row);
            _userRepo.SaveChanges();

            var response = _mapper.Map<UserResponse>(row);
            
            return CreatedAtRoute("User.GetById", new { id = row.id }, response);
        }

        [HttpPut("{id}")]
        public ActionResult<UserResponse> Update(int id, [FromBody] UpdateBody body)
        {
            var row = _userRepo.GetOne(id);
            
            if (row == null)
            {
                return NotFound();
            }

            _mapper.Map(body, row);

            _userRepo.UpdateOne(row);

            _userRepo.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<UserResponse> Delete(int id)
        {
            var row = _userRepo.GetOne(id);

            if (row == null)
            {
                return NotFound();
            }

            _userRepo.DeleteOne(row);

            _userRepo.SaveChanges();

            return NoContent();
        }
    }
}
