﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class SendMailBody
    {
        public string? TargetEmail { get; set; }

        public string? SourceEmail { get; set; }

        public string? Title { get; set; }

        public string? Content { get; set; }
    }
}
