﻿using SampleWebApi.Requests.User;
using SampleWebApi.Models;

namespace SampleWebApi.Domains
{
    public interface IUserRepo
    {
        bool SaveChanges();
        IEnumerable<User> GetMany(GetUsersQuery query);
        User GetOne(int id);
        void CreateOne(User data);
        void UpdateOne(User data);
        void DeleteOne(User data);
    }
}
