﻿using SampleWebApi.Models;
using SampleWebApi.Requests;
using SampleWebApi.Domains;

namespace SampleWebApi.Repositories
{
    public class MockProductRepo : IProductRepo
    {
        public async Task<int> CreateOne(Product cmd)
        {
            throw new NotImplementedException();
        }

        public async Task<int> DeleteOne(Product row)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Product>> GetMany(GetProductsQuery query)
        {
            return new List<Product>
            {
                new Product
                {
                    Id = 1,
                    Name = "AAA",
                    UserID = 1,
                    Price = 0,
                    ImgFileName = "",
                    Description = "",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
                new Product
                {
                    Id = 2,
                    Name = "BBB",
                    UserID = 1,
                    Price = 0,
                    ImgFileName = "",
                    Description = "",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
            };
        }

        public async Task<Product> GetOne(int id)
        {
            return new Product
            {
                Id = 1,
                Name = "AAA",
                UserID = 1,
                Price = 0,
                ImgFileName = "",
                Description = "",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now,
            };
        }

        public async Task<int> UpdateOne(Product row)
        {
            throw new NotImplementedException();
        }
    }
}
