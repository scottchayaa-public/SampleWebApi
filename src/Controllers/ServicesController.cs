﻿using Microsoft.AspNetCore.Mvc;
using NLog;
using Quartz;
using SampleWebApi.Responses;
using SampleWebApi.Services.Coingecko;
using SampleWebApi.Services.GitHub;
using SampleWebApi.Services.Machine;
using SampleWebApi.Services.Quartz.JobFactories;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]/[action]")]
    public class ServicesController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly CoingeckoService _coingeckoService;
        private readonly GithubService _githubService;
        private readonly CpuBgService _cpuBackgroundService;
        private readonly IScheduler _scheduler;

        public ServicesController(
            CoingeckoService coingeckoService, 
            GithubService githubService,
            CpuBgService cpuBackgroundService,
            IScheduler scheduler)
        {
            _coingeckoService = coingeckoService;
            _githubService = githubService;
            _cpuBackgroundService = cpuBackgroundService;
            _scheduler = scheduler;
        }

        [HttpGet]
        public async Task<IActionResult> PingCoin()
        {
            var result = await _coingeckoService.PingAsync();

            return Ok(result);
        }

        [HttpGet]
        public async Task<ActionResult<UserResponse>> GetGitHubUser()
        {
            var result = await _githubService.GetUserAsync();
            
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> StartCpuService()
        {
            await _cpuBackgroundService.StartAsync(default);

            //_logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] StartCpuService");

            return Ok(new { message = "StartCpuService." });
        }

        [HttpGet]
        public async Task<IActionResult> StopCpuService()
        {
            await _cpuBackgroundService.StopAsync(default);

            //_logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] StopCpuService");

            return Ok(new { message = "StopCpuService." });
        }


        [HttpGet]
        public async Task<IActionResult> GetCpuUsage()
        {
            var CpuUsage = _cpuBackgroundService.CpuUsage;

            return Ok(new { CpuUsage = CpuUsage });
        }

    }
}
