﻿using HtmlAgilityPack;
using Microsoft.Extensions.Options;
using SampleWebApi.Config;
using SampleWebApi.Services.Ptt.Dto;

namespace SampleWebApi.Services.Ptt
{
    /// <summary>
    /// Ptt 服務
    /// </summary>
    public class PttService
    {
        private readonly string _host = "https://www.ptt.cc";
        private readonly HttpClient _httpClient;
        private readonly AppConfig _appConfig;

        public PttService(IHttpClientFactory httpClientFactory, AppConfig appConfig)
        {
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri(_host);
            _httpClient.Timeout = TimeSpan.FromMilliseconds(3000);
            //_httpClient.DefaultRequestHeaders.Add("Accept", "application/vnd.github+json");
            //_httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + token); // 若無 Authorization, 則會有 401 錯誤
            //_httpClient.DefaultRequestHeaders.Add("User-Agent", "App_Scott"); // 若無 User-Agent, 則會有 403 錯誤

            _appConfig = appConfig;
        }

        /// <summary>
        /// 取前 3 頁資料
        /// </summary>
        public async Task<string> GetStocksAsync()
        {
            var response = await _httpClient.GetAsync("/bbs/Stock");

            if (response.IsSuccessStatusCode == false)
            {
                throw new HttpClientException(response);
            }

            var body = await response.Content.ReadAsStringAsync();

            return body;
        }

        /// <summary>
        /// Stock 版取前 n 頁資料
        /// </summary>
        public async Task<List<PttStock>> GetStocksAsync(int pages = 3)
        {
            var PttStocks = new List<PttStock>();

            var web = new HtmlWeb();

            HtmlDocument pre_doc = null;
            for (int i = 0; i < pages; i++)
            {
                HtmlDocument doc = null;

                if (i == 0)
                {
                    doc = web.Load($"{_host}/bbs/Stock");
                }
                else
                {
                    doc = pre_doc;
                }

                var nodes = doc.DocumentNode.SelectNodes("//div[contains(@class,'r-ent')]");

                foreach (var node in nodes)
                {
                    string grade = "", title = "", route = "", author = "";
                    DateTime created_at = DateTime.Now;

                    try
                    {
                        grade = node.SelectSingleNode(".//span")?.InnerHtml ?? "0";
                        title = node.SelectSingleNode(".//div[contains(@class,'title')]/a")?.InnerHtml ?? "";
                        route = node.SelectSingleNode(".//div[contains(@class,'title')]/a")?.Attributes["href"].Value ?? "";
                        author = node.SelectSingleNode(".//div[contains(@class,'author')]")?.InnerHtml ?? "";
                        created_at = DateTime.UnixEpoch.AddSeconds(Convert.ToInt32(route.Split('.')[1]));
                    }
                    catch (Exception ex) { }

                    // 若 grade 不是數字
                    if (!int.TryParse(grade, out int g))
                    {
                        grade = "0";

                        if (grade == "爆")
                        {
                            grade = "999";
                        }
                    }

                    if (!string.IsNullOrEmpty(title))
                    {
                        PttStocks.Add(new PttStock
                        {
                            Grade = Convert.ToInt32(grade),
                            Title = title,
                            Url = _host + route,
                            Author = author,
                            CreatedAt = created_at,
                        });
                    }
                }

                var top_a_nodes = doc.DocumentNode.SelectNodes("//a[contains(@class,'btn wide')]");
                var pre_doc_url = _host + top_a_nodes[1].Attributes["href"].Value; // 上頁
                pre_doc = web.Load(pre_doc_url);
            }

            return PttStocks;
        }
    }
}
