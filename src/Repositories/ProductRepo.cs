﻿using SampleWebApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using SampleWebApi.Requests;
using SampleWebApi.Domains;
using SampleWebApi.Helpers;

namespace SampleWebApi.Repositories
{
    public class ProductRepo : IProductRepo
    {
        private readonly SqlContext _context;

        public ProductRepo(SqlContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Product>> GetMany(GetProductsQuery input)
        {
            var queryAble = _context.Products.AsQueryable();

            if (input.Name != null)
            {
                queryAble = queryAble.Where(x => x.Name == input.Name);
            }
            if (input.StartPrice != null)
            {
                queryAble = queryAble.Where(x => x.Price >= input.StartPrice);
            }
            if (input.EndPrice != null)
            {
                queryAble = queryAble.Where(x => x.Price <= input.EndPrice);
            }

            var sql_str = queryAble.ToQueryString();

            var rows = await queryAble.ToListAsync(); // 語法轉換 > 執行 sql

            return rows;
        }

        public async Task<Product> GetOne(int id)
        {
            //return _context.Products.FirstOrDefault(x => x.Id == id);

            // 可防 sqlinjection 問題
            var _id = new SqlParameter("id", id);
            FormattableString sql = $"SELECT * FROM Products WHERE id = {id}";

            return await _context.Products.FromSqlInterpolated(sql).FirstOrDefaultAsync();
        }

        public async Task<int> CreateOne(Product row)
        {
            if (row == null)
            {
                throw new ArgumentException(nameof(row));
            }

            _context.Products.Add(row);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteOne(Product row)
        {
            if (row == null)
            {
                throw new ArgumentException(nameof(row));
            }

            _context.Products.Remove(row);

            return await _context.SaveChangesAsync();
        }

        public async Task<int> UpdateOne(Product row)
        {
            _context.Update(row);

            return await _context.SaveChangesAsync();
        }
    }
}
