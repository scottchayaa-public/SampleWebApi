﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NLog;
using StackExchange.Redis;
using SampleWebApi.Requests;
using SampleWebApi.Helpers;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]/[action]")]
    public class RedisController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMapper _mapper;
        private readonly IDatabase _redis;

        public RedisController(IMapper mapper, RedisClient redisClient)
        {
            _mapper = mapper;
            _redis = redisClient.GetDatabase(0);
        }

        [HttpGet]
        public async Task<IActionResult> GetKey([FromQuery] GetKeyQuery query)
        {
            var value = await _redis.StringGetAsync(query.Key);

            return Ok(new { value = value.ToString() });
        }

        [HttpPost]
        public async Task<IActionResult> SetKey([FromBody] SetKeyBody body)
        {
            await _redis.StringSetAsync(body.Key, body.Value);

            return Ok(new {});
        }
    }
}
