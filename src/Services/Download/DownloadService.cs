﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System.Net.Http;

namespace SampleWebApi.Services.Download
{
    /// <summary>
    /// 下載功能服務
    /// </summary>
    public class DownloadService
    {
        private readonly HttpClient _httpClient;

        public DownloadService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
        }

        /// <summary>
        /// 串流下載功能
        ///   return 任務ID
        /// </summary>
        public async Task<string> StreamDownloadAsync(string url, string storePath = "/downloads")
        {
            throw new NotImplementedException();
        }

        /// <summary>
        ///  取得下載狀態
        /// </summary>
        public string Status(string download_id)
        {
            throw new NotImplementedException();
        }
    }
}
