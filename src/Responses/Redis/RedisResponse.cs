﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Responses
{
    public class RedisResponse
    {
        [Required]
        [MaxLength(100)]
        public string Value { get; set; }
    }
}
