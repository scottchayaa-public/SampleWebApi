# Installation

前置作業
- (Windows) 安裝 [Docker Desktop](https://www.docker.com/products/docker-desktop/)

下載專案
```sh
git clone https://gitlab.com/scottchayaa-public/SampleWebApi.git
```

檢查 .env 配置
```sh
COMPOSE_PROJECT_NAME      # docker-compose 啟動後 container 的前綴變數
DATA_PATH_HOST            # volume 掛載的本機路徑
```

啟動 Docker 專案開發環境: MSSQL, Redis, Mongo, RabbitMQ
```sh
# 啟動環境, Create and start containers
docker-compose up -d

# 若要關閉, Stop and remove containers, networks
docker-compose down
```

開啟 .sln 專案，將偵錯目標選為 Docker  
![img](images/Debug_target_choice_Docker.png)  

偵錯模式啟動 Docker 應用程式，成功後會自動開啟 Swagger  
![img](images/本機_Docker_偵錯_swagger.png)  

Docker Desktop 會顯示本專案自動建立的 Container，若將 visual studio 關閉則 Container 會自動移除，但 image 還是會留下  
![img](images/Docker_Desktop_sameplewebapi.png)  



