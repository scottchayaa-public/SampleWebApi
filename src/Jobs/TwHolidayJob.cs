﻿using NLog;
using Quartz;
using SampleWebApi.Cache;
using SampleWebApi.Services.TwHoliday;

namespace SampleWebApi.Jobs
{
    /// <summary>
    /// 國定假日資料
    /// #緩存 #背景執行
    /// </summary>
    [DisallowConcurrentExecution]
    public class TwHolidayJob : IJob
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly TwHolidayService _service;
        private readonly AppCache _cache;

        public TwHolidayJob(TwHolidayService service, AppCache cache)
        {
            _service = service;
            _cache = cache;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            _logger.Info($"[背景排程] 國定假日緩存資料: 開始更新");

            await UpdateHolidayData();

            _logger.Info($"[背景排程] 國定假日緩存資料: 更新完成");
        }

        public async Task UpdateHolidayData()
        {
            var data = await _service.GetDataAsync();

            foreach (var holiday in data)
            {
                var date_str = holiday.date.ToString("yyyy-MM-dd");
                _cache.HolidayDict.AddOrUpdate(date_str, holiday, (key, oldValue) => holiday);
            }
        }
    }
}
