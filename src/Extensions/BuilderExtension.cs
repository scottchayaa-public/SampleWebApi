﻿using SampleWebApi.Config;

public static class BuilderExtension
{
    /// <summary>
    /// 建立 WebApplicationBuilder
    /// </summary>
    /// <param name="env">若 env 為 null, 則套用 ASPNETCORE_ENVIRONMENT。若 env 有值則注入指定環境</param>
    /// <returns></returns>
    public static WebApplicationBuilder CreateBuilder(string? env = null)
    {
        try
        {
            WebApplicationBuilder builder;

            if (env == null)
            {
                env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                Console.WriteLine($"Default ASPNETCORE_ENVIRONMENT={env}");

                builder = WebApplication.CreateBuilder(new string[] { });
            }
            else
            {
                builder = WebApplication.CreateBuilder(new WebApplicationOptions
                {
                    EnvironmentName = env
                });
            }

            // 注入 appsetting.cs 的自訂設定
            builder.Services.AddSingletonConfig<AppConfig>(builder.Configuration.GetSection(AppConfig.Position), config =>
            {
                config.Env = env;
            });

            // 注入專案所有的 class
            builder.Services.AddServices();
             
            return builder;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
            throw;
        }
    }
}
