﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class GetKeyQuery
    {
        [Required]
        [MaxLength(20)]
        public string Key { get; set; }
    }
}
