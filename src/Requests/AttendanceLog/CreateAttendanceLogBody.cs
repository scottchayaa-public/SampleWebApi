﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class CreateAttendanceLogBody
    {
        [Required]
        public string? BookName { get; set; }

        [Required]
        [Range(100, 10000, ErrorMessage = "Price 參數不合法")]
        public decimal Price { get; set; }

        [StringLength(15)]
        public string? Category { get; set; }

        [StringLength(30)]
        public string? Author { get; set; }
    }
}
