﻿using Microsoft.AspNetCore.Mvc;

public static class MvcBuilderExtension
{
    /// <summary>
    /// 自訂 Validation Exception 錯誤回傳格式
    /// </summary>
    public static IMvcBuilder ConfigureModelStateExcepiton(this IMvcBuilder builder)
    {
        builder.Services.Configure<ApiBehaviorOptions>(options =>
        {
            options.SuppressMapClientErrors = true; // 不輸出 415 錯誤 reponse 內容

            options.InvalidModelStateResponseFactory = context =>
            {
                // Request 的 Validation 例外處理
                var exception_data = new Dictionary<string, object>();
                exception_data.Add("validation_error", context.ModelState.Select(x => new {
                    field = x.Key,
                    messages = x.Value?.Errors.Select(y => y.ErrorMessage)
                }));

                // 用例外回傳格式錯誤
                throw new HttpException(StatusCodes.Status422UnprocessableEntity, "Unprocessable Entity", exception_data);

                //var result = new ValidationFailedResult(context.ModelState);
                //result.ContentTypes.Add(MediaTypeNames.Application.Json);
                //return result;
            };
        });

        return builder;
    }

}
