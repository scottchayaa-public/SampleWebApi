﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NLog;
using SampleWebApi.Requests;
using SampleWebApi.Helpers;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]/[action]")]
    public class MailController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMapper _mapper;
        private readonly RabbitMQProducer _rmq;

        public MailController(IMapper mapper, RabbitMQProducer rmq)
        {
            _mapper = mapper;
            _rmq = rmq;
        }

        [HttpPost]
        public async Task<IActionResult> Send([FromBody] SendMailBody body)
        {
            _rmq.SendMessage("mail", body);
            return Ok(body);
        }
    }
}
