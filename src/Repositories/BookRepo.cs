﻿using MongoDB.Bson;
using MongoDB.Driver;
using SampleWebApi.Domains;
using SampleWebApi.Helpers;
using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Repositories
{
    public class BookRepo : IBookRepo
    {
        private readonly IMongoCollection<Book> _book;

        public BookRepo(MongoCollection mongoCollection)
        {
            _book = mongoCollection.Books;
        }

        public async Task<IEnumerable<Book>> GetManyAsync(GetBooksQuery query)
        {
            var builder = Builders<Book>.Filter;
            
            var filter = builder.Empty;

            if (query.BookName != null)
            {
                filter &= builder.Regex("BookName", new BsonRegularExpression($".*{query.BookName}.*"));
            }
            if (query.StartPrice != null)
            {
                filter &= builder.Gt("Price", query.StartPrice);
            }
            if (query.StartPrice != null)
            {
                filter &= builder.Lt("Price", query.EndPrice);
            }
            if (query.Category != null)
            {
                filter &= builder.Eq("Category", query.Category);
            }
            if (query.Author != null)
            {
                filter &= builder.Regex("Author", new BsonRegularExpression($".*{query.Author}.*"));
            }

            var rows = await _book.Find(filter).ToListAsync();

            return rows;
        }


        public async Task<Book> GetOneAsync(string id)
        {
            var filter = Builders<Book>.Filter.Eq("Id", id);

            var row = await _book.Find(filter).FirstOrDefaultAsync();

            return row;
        }

        public async Task CreateOneAsync(Book row)
        {
            await _book.InsertOneAsync(row);
        }

        public async Task<long> UpdateOneAsync(string id, Book row)
        {
            var filter = Builders<Book>.Filter.Eq("Id", id);

            var result = await _book.ReplaceOneAsync(filter, row);

            return result.ModifiedCount;
        }

        public async Task<long> DeleteOneAsync(string id)
        {
            var filter = Builders<Book>.Filter.Eq("Id", id);

            var result = await _book.DeleteOneAsync(filter);

            return result.DeletedCount;
        }
    }
}
