# SampleWebApi
使用 .net 6 建立的基本專案  
根據 laravel 經驗整理專案常用功能  
用於快速建立服務常用的範本

1. [Installation](docs/installation.md)
1. [Docker](docs/docker.md)
1. [Nuget 套件](docs/nugets.md)
1. [appsetting 與 Config 環境設定](docs/appsetting_config.md)
1. [Log]()
1. [Route]()
1. [SQL]()
1. [Redis]()
1. [RabbitMQ]()
1. [Migration](docs/migration.md)
1. [Middleware 中介層]()
1. [Validation 資料驗證]()
1. [Exception 例外處理]()
1. [BackgroundService 背景服務]()
1. [Schedule]()
1. [Extenstion]()
1. [HttpClient]()
1. [WebScoket]()