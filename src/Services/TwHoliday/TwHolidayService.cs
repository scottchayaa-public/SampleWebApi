﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SampleWebApi.Config;
using SampleWebApi.Services.TwHoliday.Dto;

namespace SampleWebApi.Services.TwHoliday
{
    /// <summary>
    /// 政府-國定假期服務
    /// 參考資料: https://data.ntpc.gov.tw/datasets/308DCD75-6434-45BC-A95F-584DA4FED251
    /// </summary>
    public class TwHolidayService
    {
        private readonly string _host = "https://data.ntpc.gov.tw/api/v1/dataset.datastore.list?pid=308DCD75-6434-45BC-A95F-584DA4FED251";
        private readonly HttpClient _httpClient;
        private readonly AppConfig _appConfig;
        private readonly int _page_limit = 200;

        public TwHolidayService(IHttpClientFactory httpClientFactory, AppConfig appConfig)
        {
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri(_host);
            _httpClient.Timeout = TimeSpan.FromMilliseconds(3000);

            _appConfig = appConfig;
        }

        /// <summary>
        /// 取最後 200 + xxx 筆資料 (這樣一定可以取到今年)
        /// </summary>
        public async Task<List<HolidayDto>> GetDataAsync()
        {
            List<HolidayDto> result = new List<HolidayDto>();

            var response = await _httpClient.GetAsync($"{_host}");
            if (response.IsSuccessStatusCode == false)
            {
                throw new HttpClientException(response);
            }

            var body = await response.Content.ReadAsStringAsync();
            var json = JsonConvert.DeserializeObject(body) as JObject;
            var total = Convert.ToInt32(json["payload"]["total"].ToString()); // 先取得全部資料筆數

            //-----

            var page1 = total / _page_limit; //取得第 1 次執行的頁數;
            var page2 = page1 + 1; //取得第 2 次執行的頁數;

            var tasks = new Task<HttpResponseMessage>[2]
            {
                _httpClient.GetAsync($"{_host}&page_limit={_page_limit}&page_num={page1}"),
                _httpClient.GetAsync($"{_host}&page_limit={_page_limit}&page_num={page2}")
            };
            var responses = await Task.WhenAll(tasks);

            foreach (var res in responses)
            {
                if (res.IsSuccessStatusCode == false)
                {
                    throw new HttpClientException(response);
                }

                var body2 = await res.Content.ReadAsStringAsync();
                var json2 = JObject.Parse(body2);

                foreach (JObject content in (JArray)json2["payload"]["content"])
                {
                    result.Add(new HolidayDto
                    {
                        date = DateTime.Parse(content["date"].ToString()),
                        chinese = content["chinese"].ToString(),
                        isholiday = content["isholiday"].ToString() == "是" ? true : false,
                        holidaycategory = content["holidaycategory"].ToString(),
                    });
                }
            }

            return result;
        }

    }
}
