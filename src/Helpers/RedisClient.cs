﻿using Microsoft.Extensions.Options;
using SampleWebApi.Config;
using StackExchange.Redis;

namespace SampleWebApi.Helpers
{
    public class RedisClient
    {
        private readonly AppConfig _appConfig;
        private readonly ConnectionMultiplexer connectionMultiplexer;

        public RedisClient(AppConfig appConfig)
        {
            _appConfig = appConfig;

            connectionMultiplexer = ConnectionMultiplexer.Connect(_appConfig.RedisConnection);
        }

        /// <summary>
        /// 取得 Redis DB 位置
        /// </summary>
        /// <param name="db">Redis DB index</param>
        public IDatabase GetDatabase(int db = 0)
        {
            return connectionMultiplexer.GetDatabase(db);
        }
    }
}
