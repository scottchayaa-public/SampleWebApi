﻿using Quartz;

namespace SampleWebApi.Services.Quartz.Jobs
{
    [DisallowConcurrentExecution]
    public class MyJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine($"+++ MyJob: {DateTime.Now}");
                await Task.Delay(1000); // 模擬處理時間
            }
        }
    }
}
