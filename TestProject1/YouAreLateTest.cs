﻿using Microsoft.Extensions.DependencyInjection;
using SampleWebApi.Exchanges;
using SampleWebApi.Services.Quartz;
using SampleWebApi.Services.TwHoliday;
using SampleWebApi.Services.YouAreLate;

namespace TestProject1
{
    [TestClass]
    public class YouAreLateTest
    {
        private readonly string? _env = "Development"; // 指定注入環境變數
        readonly IServiceProvider _provider;

        public YouAreLateTest()
        {
            var builder = BuilderExtension.CreateBuilder(_env);
            var app = builder.Build();

            _provider = app.Services;
        }

        [TestMethod]
        public async Task GetDeptAttendanceLogHtmlAsync_test()
        {
            // Arrange
            var dept = "2-Server";
            DateTimeOffset dateTimeOffset = DateTimeOffset.UtcNow; // 取得 UtcNow 時間
            long timestamp_sec = dateTimeOffset.ToUnixTimeSeconds();

            // Act
            var service = _provider.GetRequiredService<YouAreLateService>();
            var result = await service.GetDeptAttendanceLogHtmlAsync(dept, timestamp_sec);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetDeptAttendanceLogsAsync_test()
        {
            // Arrange
            var dept = "2-Server";
            DateTimeOffset dateTimeOffset = DateTimeOffset.UtcNow; // 取得 UTC 時間
            long timestamp_sec = dateTimeOffset.ToUnixTimeSeconds();

            //var config = _provider.GetRequiredService<AppConfig>();

            // Act
            var service = _provider.GetRequiredService<YouAreLateService>();
            var result = await service.GetDeptAttendanceLogsAsync(dept, timestamp_sec);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task UpdateAttendanceLogs_test()
        {
            // Arrange
            var dept = "2-Server";
            DateTimeOffset dateTimeOffset = DateTimeOffset.UtcNow; // 取得 UTC 時間
            //DateTimeOffset dateTimeOffset = DateTimeOffset.Now.AddDays(-120); 
            long timestamp_sec = dateTimeOffset.ToUnixTimeSeconds();

            //var config = _provider.GetRequiredService<AppConfig>();
            var jobBgService = _provider.GetRequiredService<JobBgService>();

            // Act
            await jobBgService.StartAsync(default);
            var exchange = _provider.GetRequiredService<YouAreLateExchange>();
            await exchange.CreateAndUpdateAttendanceLogsAsync(dept, timestamp_sec);

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task TwHoliday_GetDataAsync_test()
        {
            // Arrange
            //var date = DateTime.ParseExact("2023-04-01", "yyyy-MM-dd", CultureInfo.InvariantCulture);
            var jobBgService = _provider.GetRequiredService<JobBgService>();
            var service = _provider.GetRequiredService<TwHolidayService>();

            // Act
            await jobBgService.StartAsync(default);
            var result = await service.GetDataAsync();

            // Assert
            Assert.IsTrue(true);
        }
    }
}
