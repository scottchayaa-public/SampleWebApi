﻿namespace SampleWebApi.Services.Ptt.Dto
{
    public class PttStock
    {
        public int Grade { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Author { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
