﻿using AutoMapper;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using SampleWebApi.Cache;
using SampleWebApi.Domains;
using SampleWebApi.Exchanges;
using SampleWebApi.Helpers;
using SampleWebApi.Jobs;
using SampleWebApi.Profiles;
using SampleWebApi.Repositories;
using SampleWebApi.Services.Coingecko;
using SampleWebApi.Services.GitHub;
using SampleWebApi.Services.Ptt;
using SampleWebApi.Services.Quartz;
using SampleWebApi.Services.Quartz.JobFactories;
using SampleWebApi.Services.TwHoliday;
using SampleWebApi.Services.YouAreLate;
using System.Reflection;

public static class ServiceCollectionExtension
{
    public static IServiceCollection AddSingletonConfig<TConfig>(this IServiceCollection services, IConfiguration section, Action<TConfig>? callback = null) where TConfig : class
    {
        services.AddSingleton(p => BindConfigInstance<TConfig>(section, callback));
        return services;
    }

    public static IServiceCollection AddScopedConfig<TConfig>(this IServiceCollection services, IConfiguration section, Action<TConfig>? callback = null) where TConfig : class
    {
        services.AddScoped(p => BindConfigInstance<TConfig>(section, callback));
        return services;
    }

    public static IServiceCollection AddTransientConfig<TConfig>(this IServiceCollection services, IConfiguration section, Action<TConfig>? callback = null) where TConfig : class
    {
        services.AddTransient(p => BindConfigInstance<TConfig>(section, callback));
        return services;
    }

    public static IServiceCollection AddConfig<TConfig>(this IServiceCollection services, IConfiguration section, ServiceLifetime lifetime) where TConfig : class
    {
        switch (lifetime)
        {
            case ServiceLifetime.Singleton:
                services.AddSingleton(p => BindConfigInstance<TConfig>(section));
                break;
            case ServiceLifetime.Scoped:
                services.AddScoped(p => BindConfigInstance<TConfig>(section));
                break;
            case ServiceLifetime.Transient:
                services.AddTransient(p => BindConfigInstance<TConfig>(section));
                break;
            default:
                throw new InvalidOperationException($"Value of enum {typeof(ServiceLifetime)}: {nameof(ServiceLifetime)} is not supported.");
        }

        return services;
    }

    private static TConfig BindConfigInstance<TConfig>(IConfiguration section, Action<TConfig>? callback = null) where TConfig : class
    {
        var instance = Activator.CreateInstance<TConfig>();
        section.Bind(instance);
        
        if (callback != null)
        {
            callback(instance);
        }
        
        return instance;
    }

    public static IServiceCollection AddSwaggerHtml(this IServiceCollection services)
    {
        var Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "SampleWebAPI",
                Version = Version
            });

            // 加入 xml 檔案到swagger，讓 api 方法可以讀取註解
            var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            c.IncludeXmlComments(xmlPath);
        });

        return services;
    }

    public static IServiceCollection AddServices(this IServiceCollection services)
    {
        
        #region Add Services to the container (DI)

        services
            .AddControllers()
            .ConfigureModelStateExcepiton();

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();

        // 注入自訂 Cache
        services.AddSingleton(new AppCache());

        // 注入 Swagger
        services.AddSwaggerHtml();

        // 注入 AutoMapper
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddSingleton(provider => 
            new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AttendanceLogProfile(provider.GetService<AppCache>()));
            }).CreateMapper()
        );

        // 注入 IHttpClientFactory (內建)
        services.AddHttpClient();

        // 注入 Quartz 服務
        services.AddSingleton<IJobFactory, JobFactory>();
        services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();

        // 注入 SQL Context
        services.AddDbContext<SqlContext>(); // mssql or mysql
        services.AddScoped<IProductRepo, ProductRepo>();
        services.AddScoped<IUserRepo, UserRepo>();

        // 注入 Mongo
        services.AddSingleton<MongoCollection>();
        services.AddSingleton<BookRepo>();
        services.AddSingleton<AttendanceLogRepo>();

        // 注入 Redis
        services.AddSingleton<RedisClient>();

        // 注入 RabbitMQ
        services.AddSingleton<RabbitMQProducer>();

        // 注入自訂 Services
        services.AddScoped<CoingeckoService>();
        services.AddScoped<GithubService>();
        services.AddScoped<PttService>();
        services.AddSingleton<YouAreLateService>();
        services.AddSingleton<YouAreLateExchange>();
        services.AddSingleton<TwHolidayService>();

        #endregion

        #region Add BackgroundServices

        //services.AddSingleton<CpuBgService>();
        //services.AddHostedService(sp => sp.GetRequiredService<CpuBgService>());

        // 注入 Job 背景服務
        services.AddSingleton<JobBgService>();
        services.AddHostedService(provider => provider.GetService<JobBgService>());

        // 注入自訂 BgService
        // ...

        #endregion


        #region Job Schedules

        //services.AddSingleton<MyJob>();
        //services.AddSingleton(new JobSchedule(
        //    jobName: "MyJob", 
        //    jobType: typeof(MyJob), 
        //    cronExpression: "0/5 * * * * ?",
        //    isStartNow: true
        //));
        services.AddSingleton<TwHolidayJob>();
        services.AddSingleton(new JobSchedule(
            jobName: "TwHolidayJob",
            jobType: typeof(TwHolidayJob),
            cronExpression: "0 0 23 12 * ?",
            isStartNow: true
        ));  // 每年 12/30 執行
        services.AddSingleton<UpdateAttendanceLogsJob>();
        services.AddSingleton(new JobSchedule(
            jobName: "UpdateAttendanceLogsJob",
            jobType: typeof(UpdateAttendanceLogsJob),
            cronExpression: "0 59 * * * ?",
            isStartNow: true
        ));  // 每個小時的 59 分執行 (須等 TwHolidayJob 完成後才能執行)

        #endregion

        return services;
    }
}
