﻿using NLog;
using Quartz;
using SampleWebApi.Exchanges;
using SampleWebApi.Services.Quartz;

namespace SampleWebApi.Jobs
{
    /// <summary>
    /// 更新上班出勤紀錄
    /// </summary>
    [DisallowConcurrentExecution]
    public class UpdateAttendanceLogsJob : IJob
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly YouAreLateExchange _exchange;

        public UpdateAttendanceLogsJob(YouAreLateExchange exchange)
        {
            _exchange = exchange;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var dept = "2-Server";
            DateTimeOffset dateTimeOffset = DateTimeOffset.Now; // 取得 +8 時區時間
            long timestamp_sec = dateTimeOffset.ToUnixTimeSeconds();

            _logger.Info($"[背景排程] 更新上班出勤紀錄: 開始更新");
            await _exchange.CreateAndUpdateAttendanceLogsAsync(dept, timestamp_sec);
            _logger.Info($"[背景排程] 更新上班出勤紀錄: 更新完成");
        }
    }
}
