﻿using NLog;
using NLog.Web;
using SampleWebApi.Middlewares;

var builder = BuilderExtension.CreateBuilder();

// 設定 NLog 讀取 appsettings
LogManager.Setup().LoadConfigurationFromAppSettings();

var logger = LogManager.GetCurrentClassLogger();


// ------------------------------------
// Add Host

//builder.Host.UseNLog();

// ------------------------------------

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// 中介 : 讀取 http request response + 捕捉全域例外
app.UseMiddleware<LogRequestResponseMiddleware>();

// 中介 : 時間
app.Use((context, next) =>
{
    context.Request.HttpContext.Items.Add("request_time", DateTime.Now);
    return next();
});

app.UseAuthorization();

app.MapControllers();


app.MapGet("/hello", () => {
    return new { message = "Hello world" };
    //throw new Exception("123");
});


#region WebSocket
//var webSocketOptions = new WebSocketOptions
//{
//    KeepAliveInterval = TimeSpan.FromMinutes(2)
//};

//app.UseWebSockets(webSocketOptions);

#endregion

logger.Info("=== Program Start ===");


app.Run();
