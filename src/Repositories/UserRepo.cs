﻿using SampleWebApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using SampleWebApi.Requests.User;
using SampleWebApi.Domains;
using SampleWebApi.Helpers;

namespace SampleWebApi.Repositories
{
    public class UserRepo : IUserRepo
    {
        private readonly SqlContext _context;
        private readonly DbSet<User> _db;

        public UserRepo(SqlContext context)
        {
            _context = context;
            _db = context.Users;
        }

        public void CreateOne(User table)
        {
            if (table == null)
            {
                throw new ArgumentException(nameof(table));
            }

            _db.Add(table);
        }

        public void DeleteOne(User table)
        {
            if (table == null)
            {
                throw new ArgumentException(nameof(table));
            }

            _db.Remove(table);
        }

        public void UpdateOne(User table)
        {
            _db.Update(table);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() >= 0;
        }

        public IEnumerable<User> GetMany(GetUsersQuery query)
        {
            var queryAble = _db.AsQueryable();

            if (query.name != null)
            {
                queryAble = queryAble.Where(x => x.name == query.name);
            }
            if (query.email != null)
            {
                queryAble = queryAble.Where(x => x.email.Contains(query.email));
            }

            var sql_str = queryAble.ToQueryString();

            var rows = queryAble.ToList(); // 語法轉換 > 執行 sql

            return rows;
        }

        public User GetOne(int id)
        {
            //return _context.FirstOrDefault(x => x.Id == id);

            // 可防 sqlinjection 問題
            var _id = new SqlParameter("id", id);
            FormattableString sql = $"SELECT * FROM User WHERE id = {id}";

            return _db.FromSqlInterpolated(sql).FirstOrDefault();
        }
    }
}
