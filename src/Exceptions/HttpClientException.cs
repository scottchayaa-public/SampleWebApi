﻿
public class HttpClientException: HttpException
{
    public HttpClientException(HttpResponseMessage response, int _StatusCode = 400, string Message = "Thirparty http request error", Exception? ex = null)
        : base(_StatusCode, Message)
    {
        StatusCode = (int)_StatusCode;

        Data.Add("error_status_code", response.StatusCode);
        Data.Add("error_body", response.Content.ReadAsStringAsync().Result);
    }
}
