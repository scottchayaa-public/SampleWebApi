﻿using AutoMapper;
using SampleWebApi.Models;
using SampleWebApi.Requests;
using SampleWebApi.Responses;

namespace SampleWebApi.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            // Source -> Target
            CreateMap<Product, ProductResponse>();
            CreateMap<CreateProductBody, Product>();
            CreateMap<UpdateProductBody, Product>();
            CreateMap<Product, UpdateProductBody>();
        }
    }
}
