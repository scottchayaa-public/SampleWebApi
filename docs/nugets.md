# Nuget 套件

MSSQL
 - Microsoft.EntityFrameworkCore.Relational
   - 延伸功能, 讓 FromSql() 可使用 sql 字串語法

Pomelo.EntityFrameworkCore.MySql
 - MySQL 套件

Log
 - NLog
 - NLog.Web.AspNetCore
 - NLog.Extensions.Logging
  - [NLog configuration with appsettings.json](https://github.com/NLog/NLog.Extensions.Logging/wiki/NLog-configuration-with-appsettings.json#logging-rule-override)
  - JsonLayout.MaxRecursionLimit : json 輸出鍵值最大階層, default = 1, 設定越大會影響效能, 假如沒設定這個, 2階層後的 json log 輸入會變成型態字串

Redis
 - StackExchange.Redis

Mongo
 - MongoDB.Driver
 
Schedule
 - Quartz
 - Quartz.Extensions.DependencyInjection
 - Quartz.Extensions.Hosting 

Queue
 - RabbitMQ.Client

AutoMapper.Extensions.Microsoft.DependencyInjection
 - class 來源和目標物件轉換的好工具, 取代 LINQ Select() 方法

HtmlAgilityPack
 - 解析 Html tag 工具, 爬蟲網頁使用

Microsoft.AspNetCore.StaticFiles
 - 裝這個為了解決 Swagger Html 網頁問題, 如果沒有, 在 Linux 環境會無法開啟 Swageer html page