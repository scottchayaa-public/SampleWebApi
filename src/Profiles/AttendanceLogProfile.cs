﻿using AutoMapper;
using SampleWebApi.Cache;
using SampleWebApi.Models;
using SampleWebApi.Services.TwHoliday.Dto;
using SampleWebApi.Services.YouAreLate.Dto;

namespace SampleWebApi.Profiles
{
    public class AttendanceLogProfile : Profile
    {
        public AttendanceLogProfile(AppCache _cache)
        {
            // Source -> Target
            CreateMap<AttendanceLogDto, AttendanceLog>()
            //.ForMember(
            //    dest => dest._id,
            //    opt => opt.MapFrom(src => $"{src.WorkId}_" + src.SearchDate.AddHours(+8).ToString("yyyyMMdd")) // 轉成 UTC+8 儲存 _id
            //)
            .ForMember(
                dest => dest.IsHoliday,
                opt => opt.MapFrom(src => IsHoliday(src.SearchDate, _cache))
            );
            //.ForMember(
            //    dest => dest.OnWorkAt,
            //    opt => opt.MapFrom(src => src.OnWorkAt.GetValueOrDefault().ToUniversalTime())
            //)
            //.ForMember(
            //    dest => dest.OffWorkAt,
            //    opt => opt.MapFrom(src => src.OffWorkAt.GetValueOrDefault().ToUniversalTime())
            //)
            //.ForMember(
            //    dest => dest.SearchDate,
            //    opt => opt.MapFrom(src => src.SearchDate.ToUniversalTime())
            //)
        }

        private bool IsHoliday(DateTime date, AppCache _cache)
        {
            HolidayDto holidayDto;
            var result = _cache.HolidayDict.TryGetValue(date.ToString("yyyy-MM-dd"), out holidayDto);
            
            if (result == false)
            { 
                return false;
            }
            else
            {
                if (holidayDto.isholiday == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
