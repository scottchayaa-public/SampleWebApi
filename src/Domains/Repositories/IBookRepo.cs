﻿using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Domains
{
    public interface IBookRepo
    {
        Task<IEnumerable<Book>> GetManyAsync(GetBooksQuery query);

        Task<Book> GetOneAsync(string id);

        Task CreateOneAsync(Book row);

        Task<long> UpdateOneAsync(string id, Book row);

        Task<long> DeleteOneAsync(string id);
    }
}
