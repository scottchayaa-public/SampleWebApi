﻿using AutoMapper;
using SampleWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using NLog;
using SampleWebApi.Requests;
using SampleWebApi.Repositories;
using System.Net;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMapper _mapper;
        private readonly BookRepo _bookRepo;

        public BooksController(IMapper mapper, BookRepo bookRepo)
        {
            _mapper = mapper;
            _bookRepo = bookRepo;
        }

        /// <summary>
        /// 查詢書籍
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> GetBookAll([FromQuery] GetBooksQuery query)
        {
            var rows = await _bookRepo.GetManyAsync(query);

            return Ok(rows);
        }

        [HttpGet("{id}", Name = "GetBookById")]
        public async Task<IActionResult> GetBookById(string id)
        {
            var row = await _bookRepo.GetOneAsync(id);

            if (row == null)
            {
                throw new HttpException(HttpStatusCode.NotFound);
            }

            return Ok(row);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBook([FromBody] CreateBookBody body)
        {
            var row = _mapper.Map<Book>(body);

            await _bookRepo.CreateOneAsync(row);

            return CreatedAtRoute("GetBookById", new { id = row.Id }, row);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBook(string id, [FromBody] UpdateBookBody body)
        {
            var row = _mapper.Map<Book>(body);

            var affects = await _bookRepo.UpdateOneAsync(id, row);

            if (affects == 0)
            {
                throw new HttpException(HttpStatusCode.NotFound);
            }

            return Ok(new { affects = affects });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBook(string id)
        {
            var affects = await _bookRepo.DeleteOneAsync(id);

            if (affects == 0)
            {
                throw new HttpException(HttpStatusCode.NotFound);
            }

            return Ok(new { affects = affects });
        }
    }
}
