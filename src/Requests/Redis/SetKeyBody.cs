﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class SetKeyBody
    {
        [Required]
        [MaxLength(20)]
        public string Key { get; set; }

        [Required]
        [MaxLength(100)]
        public string Value { get; set; }
    }
}
