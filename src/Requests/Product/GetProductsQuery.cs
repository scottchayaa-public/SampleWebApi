﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class GetProductsQuery
    {
        /// <summary>
        /// 商品名稱
        /// </summary>
        [MaxLength(50)]
        public string? Name { get; set; }

        /// <summary>
        /// 起始商品單價
        /// </summary>
        [Range(100, 100000)]
        public int? StartPrice { get; set; }

        /// <summary>
        /// 結束商品單價
        /// </summary>
        [Range(100, 100000)]
        public int? EndPrice { get; set; }
    }
}
