﻿
using System.Net;

public class HttpException : Exception
{
	public int StatusCode { get; set; }
	
	public IDictionary<string, string>? Headers { get; set; }

	public HttpException(int _StatusCode, string Message, Dictionary<string, object>? _Data = null, IDictionary<string, string>? _Headers = null, Exception? ex = null)
		: base(Message, ex)
	{
		StatusCode = _StatusCode;

        Headers = _Headers == null ? new Dictionary<string, string>() : _Headers;

        _Data = _Data ?? new Dictionary<string, object>();
		foreach (var data in _Data)
		{
			Data.Add(data.Key, data.Value);
        }
    }


    public HttpException(HttpStatusCode _StatusCode, string Message = null, Dictionary<string, object>? _Data = null, IDictionary<string, string>? _Headers = null, Exception? ex = null)
        : base(Message ?? _StatusCode.ToString(), ex)
    {
        StatusCode = (int)_StatusCode;

        Headers = _Headers == null ? new Dictionary<string, string>() : _Headers;

        _Data = _Data ?? new Dictionary<string, object>();
        foreach (var data in _Data)
        {
            Data.Add(data.Key, data.Value);
        }
    }
}
