﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SampleWebApi.Config;
using SampleWebApi.Models;

namespace SampleWebApi.Helpers
{
    public class MongoCollection
    {
        private readonly AppConfig _appconfig;
        protected readonly IMongoDatabase mongoDatabase;

        public MongoCollection(AppConfig appConfig)
        {
            _appconfig = appConfig;

            var mongoClient = new MongoClient(_appconfig.MongoConnection);

            mongoDatabase = mongoClient.GetDatabase(_appconfig.MongoDatabase);
        }


        public IMongoCollection<Book> Books { get { return mongoDatabase.GetCollection<Book>("Books"); } }
        public IMongoCollection<AttendanceLog> AttendanceLogs { get { return mongoDatabase.GetCollection<AttendanceLog>("AttendanceLogs"); } }

    }
}
