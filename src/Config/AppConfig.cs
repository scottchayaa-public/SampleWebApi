﻿namespace SampleWebApi.Config
{
    public class AppConfig
    {
        /// <summary>
        /// 設定參數讀取位置
        /// </summary>
        public const string Position = "AppConfig";

        /// <summary>
        /// 目前環境設定
        /// </summary>
        public string? Env { get; set; }

        /// <summary>
        /// 應用程式名稱
        /// </summary>
        public string? AppName { get; set; }

        /// <summary>
        /// 是否開啟讀取 reqeust response 紀錄功能
        /// </summary>
        public bool IsCatchReqResLog { get; set; } = false;

        /// <summary>
        /// 使用哪個 DB 系統 : mssql, mysql, postgresql
        /// </summary>
        public string? DBUse { get; set; }

        /// <summary>
        /// MSSQL 連線字串
        /// </summary>
        public string? MssqlConnection { get; set; }

        /// <summary>
        /// MySQL 連線字串
        /// </summary>
        public string? MysqlConnection { get; set; }

        /// <summary>
        /// MySQL 版本 : [0] major [1] minor [2] mini
        /// </summary>
        public string? MysqlVersion { get; set; }

        /// <summary>
        /// Mongo 連線字串
        /// </summary>
        public string? MongoConnection { get; set; }

        /// <summary>
        /// Mongo 資料庫名稱
        /// </summary>
        public string? MongoDatabase { get; set; }

        /// <summary>
        /// Redis 連線字串
        /// </summary>
        public string? RedisConnection { get; set; }

        /// <summary>
        /// RabbiyMQ 連線位置:Port
        /// </summary>
        public string? RmqHost { get; set; }

        /// <summary>
        /// RabbiyMQ 使用者
        /// </summary>
        public string? RmqUsername { get; set; }

        /// <summary>
        /// RabbiyMQ 密碼
        /// </summary>
        public string? RmqPassword { get; set; }

        /// <summary>
        /// 出勤服務設定
        /// </summary>
        public YouAreLateConfig YouAreLate { get; set; } = new YouAreLateConfig();

        /// <summary>
        /// MySQL 版本 : [0] major [1] minor [2] mini
        /// </summary>
        public int[] GetMysqlVersion()
        {
            var versions = MysqlVersion.Split(".").Select(v => int.Parse(v));

            return versions.ToArray();
        }
    }
}
