# Docker

docker-compose 內容包含專案所需資料環境  


若使用 docker-compose 啟動資料庫環境，並且想 sln 偵錯，則需指定 network 環境
- 如何確認要使用哪個 docker network
  ```sh
  docker network ls
  ```
- 編輯 src\Properties\launchSettings.json  
  ```json
  "DockerfileRunArguments": "--network=scottdock_web"
  ```

RD 環境部屬
```sh
cd ~/src/SampleWebApi/src

# build
docker build -t 172.31.18.20:5000/samplewebapi:latest .

# deploy
docker push 172.31.18.20:5000/samplewebapi:latest

# update && restart
cd ~/docker
docker-compose pull && docker-compose up -d
```
