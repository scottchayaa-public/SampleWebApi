﻿using SampleWebApi.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SampleWebApi.Config;

namespace SampleWebApi.Helpers
{
    public class SqlContext : DbContext
    {
        private readonly AppConfig _appConfig;

        public SqlContext(AppConfig appConfig)
        {
            _appConfig = appConfig;
        }

        /// <summary>
        ///  初始載入 DB 連線設定
        /// </summary>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            switch (_appConfig.DBUse)
            {
                case "mssql":
                    optionsBuilder.UseSqlServer(_appConfig.MssqlConnection);
                    break;
                case "mysql":
                    var v = _appConfig.GetMysqlVersion();

                    optionsBuilder.UseMySql(
                        _appConfig.MysqlConnection,
                        new MySqlServerVersion(new Version(v[0], v[1], v[2]))
                    );
                    break;
                case "postgresql":
                    throw new NotImplementedException();
                    break;
                default:
                    optionsBuilder.UseSqlServer(_appConfig.MssqlConnection);
                    break;
            }
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
