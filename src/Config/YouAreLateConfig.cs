﻿namespace SampleWebApi.Config
{
    public class YouAreLateConfig
    {
        public string Host { get; set; } = "172.31.58.11:30079";
        public int TimeOut { get; set; } = 3000;
    }
}