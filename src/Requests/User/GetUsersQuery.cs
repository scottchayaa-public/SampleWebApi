﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests.User
{
    public class GetUsersQuery
    {
        public int? id { get; set; }

        [MaxLength(20)]
        public string? name { get; set; }

        [MaxLength(250)]
        public string? email { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? updated_at { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? created_at { get; set; }
    }
}
