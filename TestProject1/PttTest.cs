﻿using Microsoft.Extensions.DependencyInjection;
using SampleWebApi.Services.Ptt;

namespace TestProject1
{
    [TestClass]
    public class PttTest
    {
        readonly IServiceProvider _provider;

        public PttTest()
        {
            var builder = BuilderExtension.CreateBuilder();
            var app = builder.Build();

            _provider = app.Services;

            //var environmentName = builder.Environment.EnvironmentName;
            //var configRoot = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            //    .AddJsonFile($"appsettings.{environmentName}.json", optional: true, reloadOnChange: true)
            //    .Build();
            //;
        }

        [TestMethod]
        public void IocContainer_GetService_RequestHandler()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddServices();

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var service = serviceProvider.GetService<PttService>();

            Assert.IsNotNull(service);
        }

        [TestMethod]
        public async Task from_builder_test()
        {
            // Arrange
            var pages = 3;

            // Act
            var service = _provider.GetRequiredService<PttService>();
            var result = await service.GetStocksAsync(pages);

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
