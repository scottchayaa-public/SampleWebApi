﻿using System.Text;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NLog;
using SampleWebApi.Config;

namespace SampleWebApi.Middlewares
{
    public class LogRequestResponseMiddleware
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly RequestDelegate _next;
        private readonly AppConfig _appConfig;

        public LogRequestResponseMiddleware(RequestDelegate next, AppConfig appConfig)
        {
            _next = next;
            _appConfig = appConfig;
        }

        public async Task Invoke(HttpContext context)
        {
            //Copy a pointer to the original response body stream
            var originalBodyStream = context.Response.Body;

            try
            {
                if (_appConfig.IsCatchReqResLog)
                {
                    await CatchReqResLog(context, originalBodyStream);
                }   
                else
                {
                    await _next(context);
                }
            }
            catch (HttpException ex)
            {
                context.Response.Body = originalBodyStream;
                await HandleExceptionAsync(context, ex);
            }
            catch (Exception ex)
            {
                context.Response.Body = originalBodyStream;
                await HandleExceptionAsync(context, ex);
            }
        }

        /// <summary>
        /// 最外層例外處理 : HttpException
        /// </summary>
        private Task HandleExceptionAsync(HttpContext context, HttpException ex)
        {
            var response = context.Response;

            response.StatusCode = ex.StatusCode;
            response.ContentType = "application/json";
            
            var ex_headers = ex.Headers ?? new Dictionary<string, string>();
            foreach (var header in ex_headers)
            {
                response.Headers.Add(header.Key, header.Value);
            }

            var responseBody = JsonConvert.SerializeObject(new
            {
                message = ex.Message,
                error_data = ex.Data,
            });

            _logger
                .WithProperty("status_code", response.StatusCode)
                .WithProperty("headers", response.Headers.ToDictionary(x => x.Key, x => x.Value))
                .WithProperty("error_data", ex.Data)
                .Error(ex.Message);

            return context.Response.WriteAsync(responseBody);
        }

        /// <summary>
        /// 最外層例外處理 : Exception
        /// </summary>
        private Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var error_message = "Internal Server Error";
            var response = context.Response;

            response.StatusCode = StatusCodes.Status500InternalServerError;
            response.ContentType = "application/json";

            var responseBody = JsonConvert.SerializeObject(new
            {
                message = error_message,
                error_data = ex.Data,
                exception = ex.ToString().Split("\n"),
            });

            _logger
                .WithProperty("status_code", response.StatusCode)
                .WithProperty("headers", context.Response.Headers.ToDictionary(x => x.Key, x => x.Value))
                .WithProperty("error_data", ex.Data)
                .WithProperty("exception", ex.ToString().Split("\n"))
                .Error(error_message);

            return context.Response.WriteAsync(responseBody);
        }

        /// <summary>
        /// 讀取 reqeust response 紀錄
        /// </summary>
        private async Task CatchReqResLog(HttpContext context, Stream originalBodyStream)
        {
            // 處理 Request 資訊
            var request = await FormatRequest(context.Request);

            //Create a new memory stream...
            using (var newResponseBodyStream = new MemoryStream())
            {
                //...and use that for the temporary response body
                context.Response.Body = newResponseBodyStream;

                // Continue down the Middleware pipeline, eventually returning to this class
                await _next(context);

                // 處理 Response 資訊
                var response = await FormatResponse(context.Response);

                // Copy the contents of the new memory stream (which contains the response) to the original stream, which is then returned to the client.
                await newResponseBodyStream.CopyToAsync(originalBodyStream);
            }
        }

        /// <summary>
        /// 擷取 http request
        /// </summary>
        private async Task<string> FormatRequest(HttpRequest request)
        {
            request.EnableBuffering();

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];

            await request.Body.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

            var requestBody = Encoding.UTF8.GetString(buffer);

            // 將串流指回開頭
            request.Body.Seek(0, SeekOrigin.Begin);

            _logger
                .WithProperty("headers", request.Headers.ToDictionary(x => x.Key, x => x.Value))
                .WithProperty("request", new
                {
                    scheme = request.Scheme,
                    host = request.Host,
                    path = request.Path,
                    query_string = request.QueryString,
                    body = requestBody,
                })
                .Trace("http request");

            return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString} {requestBody}";
        }

        /// <summary>
        /// 擷取 http response
        /// </summary>
        private async Task<string> FormatResponse(HttpResponse response)
        {
            // 將串流指回開頭
            response.Body.Seek(0, SeekOrigin.Begin);

            // 讀取回傳內容
            string responseBody = await new StreamReader(response.Body).ReadToEndAsync();

            // We need to reset the reader for the response so that the client can read it.
            response.Body.Seek(0, SeekOrigin.Begin);

            _logger
                .WithProperty("headers", response.Headers.ToDictionary(x => x.Key, x => x.Value))
                .WithProperty("response", new
                {
                    status_code = response.StatusCode,
                    body = responseBody
                })
                .Trace("http response");

            return responseBody;
        }
    }
}
