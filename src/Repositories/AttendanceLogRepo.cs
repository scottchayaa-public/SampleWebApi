﻿using MongoDB.Bson;
using MongoDB.Driver;
using SampleWebApi.Domains;
using SampleWebApi.Helpers;
using SampleWebApi.Models;
using SampleWebApi.Requests;
using System.Linq.Expressions;

namespace SampleWebApi.Repositories
{
    public class AttendanceLogRepo : IAttendanceLogRepo
    {
        private readonly IMongoCollection<AttendanceLog> _attendanceLog;

        public AttendanceLogRepo(MongoCollection mongoCollection)
        {
            _attendanceLog = mongoCollection.AttendanceLogs;

            // 創建 WorkId 索引
            var workIdIndexModel = new CreateIndexModel<AttendanceLog>(Builders<AttendanceLog>.IndexKeys.Ascending(x => x.WorkId));
            _attendanceLog.Indexes.CreateOne(workIdIndexModel);
            // 創建 OnWorkAt 索引
            var onWorkAtIndexModel = new CreateIndexModel<AttendanceLog>(Builders<AttendanceLog>.IndexKeys.Ascending(x => x.OnWorkAt));
            _attendanceLog.Indexes.CreateOne(onWorkAtIndexModel);
            // 創建 OffWorkAt 索引
            var offWorkAtIndexModel = new CreateIndexModel<AttendanceLog>(Builders<AttendanceLog>.IndexKeys.Ascending(x => x.OffWorkAt));
            _attendanceLog.Indexes.CreateOne(offWorkAtIndexModel);
            // 創建 Dept 索引
            var deptIndexModel = new CreateIndexModel<AttendanceLog>(Builders<AttendanceLog>.IndexKeys.Ascending(x => x.Dept));
            _attendanceLog.Indexes.CreateOne(deptIndexModel);
            // 創建 WorkId + Dept 索引
            var keys = Builders<AttendanceLog>.IndexKeys.Combine(
                Builders<AttendanceLog>.IndexKeys.Ascending(x => x.WorkId),
                Builders<AttendanceLog>.IndexKeys.Ascending(x => x.Dept)
            );
            _attendanceLog.Indexes.CreateOne(new CreateIndexModel<AttendanceLog>(keys));
        }

        public async Task<IEnumerable<AttendanceLog>> GetManyAsync(GetAttendanceLogsQuery query)
        {
            var builder = Builders<AttendanceLog>.Filter;
            
            var filter = builder.Empty;

            if (query.Author != null)
            {
                filter &= builder.Regex("Author", new BsonRegularExpression($".*{query.Author}.*"));
            }

            var rows = await _attendanceLog.Find(filter).ToListAsync();

            return rows;
        }


        public async Task<AttendanceLog> GetOneAsync(string id)
        {
            var filter = Builders<AttendanceLog>.Filter.Eq("Id", id);

            var row = await _attendanceLog.Find(filter).FirstOrDefaultAsync();

            return row;
        }

        public async Task CreateOneAsync(AttendanceLog row)
        {
            await _attendanceLog.InsertOneAsync(row);
        }

        public async Task CreateAndUpdateManyAsync2(List<AttendanceLog> rows)
        {
            var writeModels = new List<WriteModel<AttendanceLog>>();

            var properties = typeof(AttendanceLog).GetProperties();

            foreach (var row in rows)
            {
                //writeModels.Add(new UpdateOneModel<AttendanceLog>(
                //    Builders<AttendanceLog>.Filter.Eq(x => x._id, row._id),
                //    Builders<AttendanceLog>.Update
                //        .Set(x => x.WorkId, row.WorkId)
                //        .Set(x => x.Dept, row.Dept)
                //        .Set(x => x.OnWorkAt, row.OnWorkAt)
                //        .Set(x => x.OffWorkAt, row.OffWorkAt)
                //        .Set(x => x.IsSpecialDay, row.IsSpecialDay)
                //        .Set(x => x.IsHoliday, row.IsHoliday)
                //) {
                //    IsUpsert = true
                //});
            }

            var result = await _attendanceLog.BulkWriteAsync(writeModels);
        }

        public async Task CreateAndUpdateManyAsync(List<AttendanceLog> rows)
        {
            var writeModels = new List<WriteModel<AttendanceLog>>();

            foreach (var row in rows)
            {
                //var filter = Builders<AttendanceLog>.Filter.Eq(x => x._id, row._id);
                var filter = Builders<AttendanceLog>.Filter.Eq(x => x.WorkId, row.WorkId) & Builders<AttendanceLog>.Filter.Eq(x => x.SearchDate, row.SearchDate);
                var update = CreateUpdateDefinition(row);

                writeModels.Add(new UpdateOneModel<AttendanceLog>(filter, update)
                {
                    IsUpsert = true
                });
            }

            var result = await _attendanceLog.BulkWriteAsync(writeModels);
        }

        private UpdateDefinition<T> CreateUpdateDefinition<T>(T row)
        {
            var properties = typeof(T).GetProperties()
                .Where(p => p.Name != "_id"); // 排除主鍵

            var update = Builders<T>.Update;
            UpdateDefinition<T> update2 = null;
            foreach (var property in properties)
            {
                var value = property.GetValue(row);

                //var lambda = CreateLambdaExpression<AttendanceLog>(property);

                if (update2 == null)
                {
                    update2 = update.Set(property.Name, value);
                }

                update2 = update2.Set(property.Name, value);
            }

            return update2;
        }

        //private Expression<Func<T, object>> CreateLambdaExpression<T>(PropertyInfo property)
        //{
        //    var parameter = Expression.Parameter(typeof(T), "x");
        //    var propertyAccess = Expression.Property(parameter, property);
        //    var convertExpression = Expression.Convert(propertyAccess, typeof(object));

        //    return Expression.Lambda<Func<T, object>>(convertExpression, parameter);
        //}

        public async Task<long> UpdateOneAsync(string id, AttendanceLog row)
        {
            var filter = Builders<AttendanceLog>.Filter.Eq("Id", id);

            var result = await _attendanceLog.ReplaceOneAsync(filter, row);

            return result.ModifiedCount;
        }

        public async Task<long> DeleteOneAsync(string id)
        {
            var filter = Builders<AttendanceLog>.Filter.Eq("Id", id);

            var result = await _attendanceLog.DeleteOneAsync(filter);

            return result.DeletedCount;
        }
    }
}
