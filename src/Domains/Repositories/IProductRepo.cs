﻿using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Domains
{
    public interface IProductRepo
    {
        Task<IEnumerable<Product>> GetMany(GetProductsQuery query);

        Task<Product> GetOne(int id);

        Task<int> CreateOne(Product row);

        Task<int> UpdateOne(Product row);

        Task<int> DeleteOne(Product row);
    }
}
