﻿using Microsoft.Net.Http.Headers;
using System.Net.Http;

namespace SampleWebApi.Services.Coingecko
{
    /// <summary>
    /// Coingecko API Document
    /// https://www.coingecko.com/zh-tw/api/documentation
    /// </summary>
    public class CoingeckoService
    {
        private readonly HttpClient _httpClient;

        public CoingeckoService(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri("https://api.coingecko.com/api/v3");
            _httpClient.Timeout = TimeSpan.FromMilliseconds(3000);
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public async Task<string> PingAsync()
        {
            var response = await _httpClient.GetAsync("/ping");

            var body = await response.Content.ReadAsStringAsync();

            return body;
        }

    }
}
