﻿using NLog;
using SampleWebApi.Services.YouAreLate;
using System.Diagnostics;

namespace SampleWebApi.BackgroundServices
{
    public sealed class YouAreLateBackgroundService : BackgroundService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly YouAreLateService _youAreLateService;

        public bool IsRunning { get; set; } = false;

        /// <summary>
        /// 多久更新一次資料 (ms)
        /// </summary>
        public int UpdateFreq { get; set; } = 3000;

        public YouAreLateBackgroundService(YouAreLateService youAreLateService)
        {
            _youAreLateService = youAreLateService;
        }

        /// <summary>
        /// 新增出勤紀錄
        /// </summary>
        private async Task CreateAttendenceLogs()
        {
            var dept = "2-Server";
            DateTimeOffset dateTimeOffset = DateTimeOffset.Now; // 取得 +8 時區時間
            long timestamp_sec = dateTimeOffset.ToUnixTimeSeconds();

            var result = await _youAreLateService.GetDeptAttendanceLogsAsync(dept, timestamp_sec);

            // repo 儲存
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (IsRunning)
            {
                _logger.Warn($"[{Thread.CurrentThread.ManagedThreadId}] YouAreLateBackgroundService is still Running !!!");
                return;
            }

            IsRunning = true;

            try
            {
                CreateAttendenceLogs();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }

            IsRunning = false;

            _logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] YouAreLateBackgroundService.ExecuteAsync() END ...");
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] YouAreLateBackgroundService.StopAsync()");

            await base.StopAsync(stoppingToken);
        }
    }
}
