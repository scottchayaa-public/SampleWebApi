﻿
using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class GetAttendanceLogsQuery
    {
        public string? BookName { get; set; }

        [Range(100, 10000, ErrorMessage = "Price 參數不合法")]
        public decimal? StartPrice { get; set; }

        [Range(100, 10000, ErrorMessage = "Price 參數不合法")]
        public decimal? EndPrice { get; set; }

        [StringLength(15)]
        public string? Category { get; set; }

        [StringLength(30)]
        public string? Author { get; set; }
    }
}
