﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Models
{
    public class User
    {
        [Key]
        public int id {get; set; }

        [Required]
        [MaxLength(20)]
        public string name {get; set; }

        [Required]
        [MaxLength(250)]
        public string email {get; set; }

        [Required]
        [MaxLength(10)]
        public string password {get; set; }

        [MaxLength(250)]
        public string? address { get; set; }

        [MaxLength(20)]
        public string? phone { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? updated_at { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? created_at { get; set; }
    }
}
