﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Models
{
    /// <summary>
    /// 商品資料
    /// </summary>
    [Index(nameof(UserID))]
    [Index(nameof(CreatedAt))]
    public class Product
    {
        [Key]
        public int Id { get; set; }
        
        /// <summary>
        /// 建立者ID
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// 商品名稱
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string? Name { get; set; }

        /// <summary>
        /// 商品單價
        /// </summary>
        [Required]
        [Range(100, 100000)]
        public int Price { get; set; }

        /// <summary>
        /// 商品描述
        /// </summary>
        [MaxLength(250)]
        public string? Description { get; set; }

        /// <summary>
        /// 商品圖片檔名
        /// </summary>
        [MaxLength(250)]
        public string? ImgFileName { get; set; }

        /// <summary>
        /// 更新時間
        /// </summary>
        [Timestamp]
        public DateTime? UpdatedAt { get; set; }

        /// <summary>
        /// 建立時間
        /// </summary>
        [Timestamp]
        public DateTime? CreatedAt { get; set; }
    }
}
