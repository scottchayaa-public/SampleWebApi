﻿namespace SampleWebApi.Services.YouAreLate.Dto
{
    public class AttendanceLogDto
    {
        /// <summary>
        /// 工號
        /// </summary>
        public int WorkId { get; set; }
        /// <summary>
        /// 部門代碼
        /// </summary>
        public string? Dept { get; set; }
        /// <summary>
        /// 上班打卡時間
        /// </summary>
        public DateTime? OnWorkAt { get; set; }
        /// <summary>
        /// 下班打卡時間
        /// </summary>
        public DateTime? OffWorkAt { get; set; }
        /// <summary>
        /// 上次上班打卡時間
        /// </summary>
        public DateTime? LastOnWorkAt { get; set; }
        /// <summary>
        /// 上次下班打卡時間
        /// </summary>
        public DateTime? LastOffWorkAt { get; set; }
        /// <summary>
        /// 查詢那天的日期
        /// </summary>
        public DateTime SearchDate { get; set; }
    }
}
