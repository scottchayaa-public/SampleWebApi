﻿using System.ComponentModel.DataAnnotations;

namespace SampleWebApi.Requests
{
    public class GetBookByIdRoute
    {
        [Required]
        [StringLength(24, MinimumLength = 24)]
        public string id { get; set; }
    }
}
