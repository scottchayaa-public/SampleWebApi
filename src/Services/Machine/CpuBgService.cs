﻿using NLog;
using System.Diagnostics;

namespace SampleWebApi.Services.Machine
{
    public sealed class CpuBgService : BackgroundService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public bool IsRunning { get; set; } = false;

        /// <summary>
        /// CPU 目前使用率
        /// </summary>
        public double CpuUsage { get; set; }

        /// <summary>
        /// 多久更新一次資料 (ms)
        /// </summary>
        public int UpdateFreq { get; set; } = 3000;

        public CpuBgService()
        {

        }

        /// <summary>
        /// 取得 CPU 使用率
        /// </summary>
        private async Task<double> GetCpuFromProcess()
        {
            var startTime = DateTime.UtcNow;
            var startCpuUsage = Process.GetCurrentProcess().TotalProcessorTime;

            await Task.Delay(500);

            var endTime = DateTime.UtcNow;
            var endCpuUsage = Process.GetCurrentProcess().TotalProcessorTime;
            var cpuUsedMs = (endCpuUsage - startCpuUsage).TotalMilliseconds;
            var totalMsPassed = (endTime - startTime).TotalMilliseconds;
            var cpuUsageTotal = cpuUsedMs / (Environment.ProcessorCount * totalMsPassed);
            return cpuUsageTotal * 100;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (IsRunning)
            {
                _logger.Warn($"[{Thread.CurrentThread.ManagedThreadId}] CpuBackgroundService is still Running !!!");
                return;
            }

            IsRunning = true;

            try
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    CpuUsage = await GetCpuFromProcess();
                    _logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] CPU: {CpuUsage}%, IsRunning: {IsRunning}");

                    await Task.Delay(TimeSpan.FromMilliseconds(UpdateFreq), stoppingToken);

                    //throw new Exception("未預期錯誤");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());

                // Terminates this process and returns an exit code to the operating system.
                // This is required to avoid the 'BackgroundServiceExceptionBehavior', which
                // performs one of two scenarios:
                // 1. When set to "Ignore": will do nothing at all, errors cause zombie services.
                // 2. When set to "StopHost": will cleanly stop the host, and log errors.
                //
                // In order for the Windows Service Management system to leverage configured
                // recovery options, we need to terminate the process with a non-zero exit code.

                ///Environment.Exit(1);
            }

            IsRunning = false;

            _logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] CpuBackgroundService.ExecuteAsync() END ...");
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.Info($"[{Thread.CurrentThread.ManagedThreadId}] CpuBackgroundService.StopAsync()");

            await base.StopAsync(stoppingToken);
        }
    }
}
