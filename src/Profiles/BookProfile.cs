﻿using AutoMapper;
using SampleWebApi.Models;
using SampleWebApi.Responses;
using SampleWebApi.Requests;

namespace SampleWebApi.Profiles
{
    public class BookProfile : Profile
    {
        public BookProfile()
        {
            // Source -> Target
            CreateMap<Book, BookResponse>();
            CreateMap<CreateAttendanceLogBody, Book>();
            CreateMap<UpdateAttendanceLogBody, Book>();
            CreateMap<Book, UpdateAttendanceLogBody>();
        }
    }
}
