﻿using HtmlAgilityPack;
using NLog;
using SampleWebApi.Config;
using SampleWebApi.Services.YouAreLate.Dto;
using System;
using System.Globalization;

namespace SampleWebApi.Services.YouAreLate
{
    /// <summary>
    /// 公司出勤服務
    /// </summary>
    public class YouAreLateService
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly string _host;
        private readonly HttpClient _httpClient;
        private readonly string _datetime_format = "yyyy/MM/dd-HH:mm"; // 定義日期交換格式

        public YouAreLateService(IHttpClientFactory httpClientFactory, AppConfig appConfig)
        {
            _host = "http://" + appConfig.YouAreLate.Host;
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri(_host);
            _httpClient.Timeout = TimeSpan.FromMilliseconds(appConfig.YouAreLate.TimeOut);
        }

        /// <summary>
        /// 取得出勤資料(html)
        /// </summary>
        public async Task<string> GetDeptAttendanceLogHtmlAsync(string dept = "2-Server", long timestamp_sec = 0)
        {
            var response = await _httpClient.GetAsync($"/Home/PostHistoryGrid?Depart={dept}&_={timestamp_sec}");

            if (response.IsSuccessStatusCode == false)
            {
                throw new HttpClientException(response);
            }

            var body = await response.Content.ReadAsStringAsync();

            return body;
        }

        /// <summary>
        /// 取得出勤資料 (UTC+0)
        /// </summary>
        public async Task<List<AttendanceLogDto>> GetDeptAttendanceLogsAsync(string dept = "2-Server", long utc_timestamp_sec = 0)
        {
            var AttendanceLogs = new List<AttendanceLogDto>();

            #region timestamp validation
            DateTime time = DateTime.UtcNow.AddHours(+8);
            if (utc_timestamp_sec != 0)
            {
                time = DateTime.UnixEpoch.AddSeconds(utc_timestamp_sec).AddHours(+8); // 轉成當地查詢的時間
            }
            var date_str = time.ToString("yyyy/MM/dd"); 
            //var SearchDate = DateTime.UnixEpoch.AddSeconds(utc_timestamp_sec); 
            #endregion

            var web = new HtmlWeb();

            var doc = web.Load($"{_host}/Home/PostHistoryGrid?Depart={dept}&_={utc_timestamp_sec}");

            var nodes = doc.DocumentNode.SelectNodes("//tbody/tr");
            
            foreach (var node in nodes)
            {
                var attendanceLog = new AttendanceLogDto();

                try
                {
                    var tds = node.SelectNodes(".//td");

                    // 取得上次出勤的年份
                    var last_year = DateTime.Now.Year;
                    var last_month = Convert.ToInt32(tds[4].InnerText.Split("-")[0].Split("/")[0]); // 取得上次出勤的月份
                    var now_month = DateTime.Now.Month;
                    if (last_month - now_month > 0) // 如果相減大於 0 表示跨年了
                    {
                        last_year -= 1;
                    }

                    /*
                     * 原始資料範例 :
                     *  <td class="col-1">203</td>
                        <td class="col-1">Server</td>
                        <td class="col-1">09:48</td>
                        <td class="col-1"></td>
                        <td class="col-1">03/29-09:43</td>
                        <td class="col-1">03/29-20:14</td>
                     */
                    attendanceLog.WorkId = Convert.ToInt32(tds[0].InnerText);
                    attendanceLog.Dept = tds[1].InnerText != "" ? tds[1].InnerText : null;
                    
                    attendanceLog.OnWorkAt = tds[2].InnerText != "" ? DateTime.ParseExact($"{date_str}-{tds[2].InnerText}", _datetime_format, CultureInfo.InvariantCulture).AddHours(-8) : null;
                    attendanceLog.OnWorkAt = attendanceLog.OnWorkAt != null ? DateTime.SpecifyKind(attendanceLog.OnWorkAt.GetValueOrDefault(), DateTimeKind.Utc) : null;

                    attendanceLog.OffWorkAt = tds[3].InnerText != "" ? DateTime.ParseExact($"{date_str}-{tds[3].InnerText}", _datetime_format, CultureInfo.InvariantCulture).AddHours(-8) : null;
                    attendanceLog.OffWorkAt = attendanceLog.OffWorkAt != null ? DateTime.SpecifyKind(attendanceLog.OffWorkAt.GetValueOrDefault(), DateTimeKind.Utc) : null;
                    
                    attendanceLog.LastOnWorkAt = tds[4].InnerText != "" ? DateTime.ParseExact($"{last_year}/{tds[4].InnerText}", _datetime_format, CultureInfo.InvariantCulture).AddHours(-8) : null;
                    attendanceLog.LastOnWorkAt = attendanceLog.LastOnWorkAt != null ? DateTime.SpecifyKind(attendanceLog.LastOnWorkAt.GetValueOrDefault(), DateTimeKind.Utc) : null;
                    
                    attendanceLog.LastOffWorkAt = tds[5].InnerText != "" ? DateTime.ParseExact($"{last_year}/{tds[5].InnerText}", _datetime_format, CultureInfo.InvariantCulture).AddHours(-8) : null;
                    attendanceLog.LastOffWorkAt = attendanceLog.LastOffWorkAt != null ? DateTime.SpecifyKind(attendanceLog.LastOffWorkAt.GetValueOrDefault(), DateTimeKind.Utc) : null;

                    attendanceLog.SearchDate = time.Date.AddHours(-8); // 時間記錄還是需要UTC
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }

                AttendanceLogs.Add(attendanceLog);
            }

            return AttendanceLogs;
        }
    }
}
