﻿using AutoMapper;
using SampleWebApi.Models;
using SampleWebApi.Requests.User;
using SampleWebApi.Responses;

namespace SampleWebApi.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            // Source -> Target
            CreateMap<User, UserResponse>();
            CreateMap<CreateBody, User>();
            CreateMap<UpdateBody, User>();
            CreateMap<User, UpdateBody>();
        }
    }
}
