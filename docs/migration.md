
MSSQL Migration
若專案有 2 個 DBContext 的情況時，需 -c 指定 migration 目標
可透過 DBUse 指定 mssql 和 mysql ，但產出來的 migration 檔案內容會有差異，轉換 DB 前需要先清除 migration 檔案
```sh
# 刪除資料庫
dotnet ef database drop -f

# 一開始根據 Context DbSet 建立所有 tables
dotnet ef migrations add -c SqlContext Initial

# 之後若 user table 追加欄位時, migration 套件會自動比較上次的 model 結構產出新 migration 檔案
dotnet ef migrations add -c SqlContext AddUserAddress
dotnet ef migrations add -c SqlContext AddUserPhone
dotnet ef migrations add -c SqlContext Update20221229

# 更新, 根據 Program.cs 設定 DBContext 使用的連線 Update 結構到目標 DB
dotnet ef database update  -c SqlContext

# 移除
dotnet ef migrations remove -c SqlContext
```