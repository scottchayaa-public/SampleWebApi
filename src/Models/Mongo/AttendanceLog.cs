﻿using MongoDB.Bson.Serialization.Attributes;

namespace SampleWebApi.Models
{
    /// <summary>
    /// 今天出勤資料
    /// </summary>
    public class AttendanceLog
    {
        /// <summary>
        /// Format : {WorkId}_{YYYYmmdd}
        /// </summary>
        [BsonId]
        public string _id { get; set; }

        /// <summary>
        /// 工號
        /// </summary>
        public int WorkId { get; set; }

        /// <summary>
        /// 部門代碼
        /// </summary>
        public string? Dept { get; set; }

        /// <summary>
        /// 上班打卡時間
        /// </summary>
        public DateTime? OnWorkAt { get; set; }

        /// <summary>
        /// 下班打卡時間
        /// </summary>
        public DateTime? OffWorkAt { get; set; }

        /// <summary>
        /// 是否請假/特休
        /// </summary>
        public bool IsSpecialDay { get; set; }

        /// <summary>
        /// 是否為國定假日
        /// </summary>
        public bool IsHoliday { get; set; }

        /// <summary>
        /// 查詢那天的日期
        /// </summary>
        public DateTime SearchDate { get; set; }
    }
}
