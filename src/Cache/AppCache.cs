﻿using SampleWebApi.Services.TwHoliday.Dto;
using System.Collections.Concurrent;

namespace SampleWebApi.Cache
{
    public class AppCache
    {
        public ConcurrentDictionary<string, HolidayDto> HolidayDict { get; set; } = new ConcurrentDictionary<string, HolidayDto>();
    }
}
