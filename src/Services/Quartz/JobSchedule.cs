﻿using System.ComponentModel;

namespace SampleWebApi.Services.Quartz
{
    public class JobSchedule
    {
        public JobSchedule(string jobName, Type jobType, string cronExpression, bool isStartNow = false)
        {
            JobType = jobType ?? throw new ArgumentNullException(nameof(jobType));
            CronExpression = cronExpression ?? throw new ArgumentNullException(nameof(cronExpression));
            JobName = jobName ?? throw new ArgumentNullException(nameof(jobName));
            IsStartNow = isStartNow;
        }

        /// <summary>
        /// Job識別名稱
        /// </summary>
        public string JobName { get; private set; }

        /// <summary>
        /// Job型別
        /// </summary>
        public Type JobType { get; private set; }

        /// <summary>
        /// 啟動時是否立刻執行
        /// </summary>
        public bool IsStartNow { get; private set; }

        /// <summary>
        /// Cron表示式
        /// </summary>
        public string CronExpression { get; private set; }

        /// <summary>
        /// Job狀態
        /// </summary>
        public JobStatus JobStatus { get; set; } = JobStatus.Init;
    }

    public enum JobStatus : byte
    {
        [Description("初始化")]
        Init = 0,
        [Description("已排程")]
        Scheduled = 1,
        [Description("執行中")]
        Running = 2,
        [Description("已停止")]
        Stopped = 3,
    }
}
