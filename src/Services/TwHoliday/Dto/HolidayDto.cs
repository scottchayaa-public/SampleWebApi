﻿namespace SampleWebApi.Services.TwHoliday.Dto
{
    public class HolidayDto
    {
        public DateTime date { get; set; }
        public string chinese { get; set; }
        /// <summary>
        /// 是否假日, true: 是, false: 有可能補休 
        /// </summary>
        public bool isholiday { get; set; }
        public string holidaycategory { get; set; }
    }
}
