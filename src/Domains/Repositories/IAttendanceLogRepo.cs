﻿using SampleWebApi.Models;
using SampleWebApi.Requests;

namespace SampleWebApi.Domains
{
    public interface IAttendanceLogRepo
    {
        Task<IEnumerable<AttendanceLog>> GetManyAsync(GetAttendanceLogsQuery query);

        Task<AttendanceLog> GetOneAsync(string id);

        Task CreateOneAsync(AttendanceLog row);

        Task<long> UpdateOneAsync(string id, AttendanceLog row);

        Task<long> DeleteOneAsync(string id);
    }
}
