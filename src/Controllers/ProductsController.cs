﻿using AutoMapper;
using SampleWebApi.Models;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using NLog;
using SampleWebApi.Responses;
using SampleWebApi.Requests;
using SampleWebApi.Domains;
using System.Net;

namespace SampleWebApi.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IProductRepo _productRepo;
        private readonly IMapper _mapper;

        public ProductsController(IProductRepo productRepo, IMapper mapper)
        {
            _logger
                .WithProperty("type", "ctor")
                .Info("ctor");

            _productRepo = productRepo;
            _mapper = mapper;
        }

        /// <summary>
        /// 查詢多筆商品
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductResponse>>> GetProducts([FromQuery] GetProductsQuery query)
        {
            #region logger output test
            _logger.Debug("+++++++++++++++++++++++++++++");
            _logger
                .WithProperty("type", "controller")
                .WithProperty("method", "GetProductAll")
                .WithProperty("headers", new { aa = 11, bb = 22, cc = "https://www.google.com" })
                .Info($"{this.GetType()}");
            #endregion

            var products = await _productRepo.GetMany(query);

            return Ok(_mapper.Map<IEnumerable<ProductResponse>>(products));
        }

        /// <summary>
        /// 查詢單筆商品
        /// </summary>
        [HttpGet("{id}", Name = "GetProductById")]
        public ActionResult<ProductResponse> GetProductById(int id)
        {
            var row = _productRepo.GetOne(id);

            if (row == null)
            {
                throw new HttpException(HttpStatusCode.NotFound);
            }

            return Ok(_mapper.Map<ProductResponse>(row));
        }

        /// <summary>
        /// 建立商品
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateProduct([FromBody] CreateProductBody body)
        {
            var row = _mapper.Map<Product>(body);

            await _productRepo.CreateOne(row);

            var response = _mapper.Map<ProductResponse>(row);

            return CreatedAtRoute("GetProductById", new { id = response.Id }, response);
        }

        /// <summary>
        /// 更新商品
        /// </summary>
        [HttpPut("{id}")]
        public async Task<ActionResult<ProductResponse>> UpdateProduct(int id, [FromBody] UpdateProductBody body)
        {
            var row = await _productRepo.GetOne(id);

            if (row == null)
            {
                throw new HttpException(HttpStatusCode.NotFound);
            }

            _mapper.Map(body, row);

            await _productRepo.UpdateOne(row);

            return NoContent();
        }

        // Patch 寫法很少用
        [HttpPatch("{id}")]
        public async Task<ActionResult<ProductResponse>> PartialProductUpdate(int id, JsonPatchDocument<UpdateProductBody> body)
        {
            var row = await _productRepo.GetOne(id);

            if (row == null)
            {
                return NotFound();
            }

            var rowToPatch = _mapper.Map<UpdateProductBody>(row);

            body.ApplyTo(rowToPatch, ModelState);

            if (!TryValidateModel(rowToPatch))
            {
                return ValidationProblem(ModelState);
            }

            _mapper.Map(rowToPatch, row);

            await _productRepo.UpdateOne(row);

            return NoContent();
        }

        /// <summary>
        /// 刪除商品
        /// </summary>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductResponse>> DeleteProduct(int id)
        {
            var row = await _productRepo.GetOne(id);

            if (row == null)
            {
                return NotFound();
            }

            await _productRepo.DeleteOne(row);

            return NoContent();
        }
    }
}

