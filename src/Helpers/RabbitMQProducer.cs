﻿using Microsoft.Extensions.Options;
using SampleWebApi.Config;
using RabbitMQ.Client;
using System.Text;
using Newtonsoft.Json;

namespace SampleWebApi.Helpers
{
    public class RabbitMQProducer
    {
        private readonly AppConfig _appConfig;
        private readonly IConnection _connection;

        public RabbitMQProducer(AppConfig appConfig)
        {
            _appConfig = appConfig;

            var factory = new ConnectionFactory
            {
                HostName = _appConfig.RmqHost
            };

            _connection = factory.CreateConnection();
        }

        /// <summary>
        /// Send Rmq Message
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="routingKey"></param>
        /// <param name="message"></param>
        /// <param name="exchange">Exchange Name</param>
        public void SendMessage<T>(string routingKey, T message, string exchange = "")
        {
            using var channel = _connection.CreateModel();
            
            channel.QueueDeclare(routingKey, exclusive: false);

            //Serialize the message
            var json = JsonConvert.SerializeObject(message);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(exchange, routingKey, body: body);
        }
    }
}
