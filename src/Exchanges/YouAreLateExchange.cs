﻿using AutoMapper;
using Newtonsoft.Json;
using NLog;
using SampleWebApi.Cache;
using SampleWebApi.Jobs;
using SampleWebApi.Models;
using SampleWebApi.Repositories;
using SampleWebApi.Services.Quartz;
using SampleWebApi.Services.YouAreLate;

namespace SampleWebApi.Exchanges
{
    public class YouAreLateExchange
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly IMapper _mapper;
        private readonly YouAreLateService _youAreLateService;
        private readonly AttendanceLogRepo _attendanceLogRepo;
        private readonly JobBgService _jobBgService;
        private readonly AppCache _cache;

        public YouAreLateExchange(IMapper mapper, YouAreLateService youAreLateService, AttendanceLogRepo attendanceLogRepo, JobBgService jobBgService, AppCache cache)
        {
            _mapper = mapper;
            _youAreLateService = youAreLateService;
            _attendanceLogRepo = attendanceLogRepo;
            _jobBgService = jobBgService;
            _cache = cache;
        }

        /// <summary>
        /// 新增/更新出勤時間 (今日+上次)
        /// </summary>
        /// <param name="dept">查詢部門</param>
        /// <param name="utc_timestamp_sec">查詢日期 => 目前這個值根本沒作用，系統還是只會回傳當日</param>
        /// <returns></returns>
        public async Task CreateAndUpdateAttendanceLogsAsync(string dept = "2-Server", long utc_timestamp_sec = 0)
        {
            var twHolidayJob_status = _jobBgService.GetJobScheduleStatus<TwHolidayJob>();

            if (twHolidayJob_status != JobStatus.Scheduled)
            {
                _logger.Error("TwHolidayJob 還沒執行過, 國定假日判斷功能會失效");
            }

            // 更新今日
            var dtos = await _youAreLateService.GetDeptAttendanceLogsAsync(dept, utc_timestamp_sec);
            var rows = _mapper.Map<List<AttendanceLog>>(dtos);

            // 更新上次
            var last_dtos = dtos.ToList();
            last_dtos.ForEach(dto =>
            {
                dto.OnWorkAt = dto.LastOnWorkAt;
                dto.OffWorkAt = dto.LastOffWorkAt;
                dto.SearchDate = dto.LastOnWorkAt.GetValueOrDefault().Date.AddHours(-8);
            });
            var last_rows = _mapper.Map<List<AttendanceLog>>(last_dtos);

            _logger.Debug($"更新上次出勤 : {JsonConvert.SerializeObject(last_rows)}");
            await _attendanceLogRepo.CreateAndUpdateManyAsync(last_rows);

            _logger.Debug($"更新今日出勤 : {JsonConvert.SerializeObject(rows)}");
            await _attendanceLogRepo.CreateAndUpdateManyAsync(rows);
        }

    }
}
